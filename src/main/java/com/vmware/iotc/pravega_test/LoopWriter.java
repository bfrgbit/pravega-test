package com.vmware.iotc.pravega_test;

import java.net.URI;
import java.util.ArrayList;

import org.apache.commons.cli.*;

public class LoopWriter extends SimpleWriter {
    public LoopWriter(String scope, String streamName, URI controllerURI) {
        super(scope, streamName, controllerURI);
    }

    public static void main(String[] args) {
        Options options = getOptions();
        CommandLine cmd = null;
        try {
            cmd = parseCommandLineArgs(options, args);
        } catch (ParseException e) {
            System.out.format("%s.%n", e.getMessage());
            final HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("HelloWorldWriter", options);
            System.exit(1);
        }

        final String scope = cmd.getOptionValue("scope") == null ? Constants.DEFAULT_SCOPE
                : cmd.getOptionValue("scope");
        final String streamName = cmd.getOptionValue("name") == null ? Constants.DEFAULT_STREAM_NAME
                : cmd.getOptionValue("name");
        final String uriString = cmd.getOptionValue("uri") == null ? Constants.DEFAULT_CONTROLLER_URI
                : cmd.getOptionValue("uri");
        final URI controllerURI = URI.create(uriString);

        SimpleWriter writer = new SimpleWriter(scope, streamName, controllerURI);

        final String routingKey = cmd.getOptionValue("routingKey") == null ? Constants.DEFAULT_ROUTING_KEY
                : cmd.getOptionValue("routingKey");
        final int num_messages = cmd.getOptionValue("num_messages") == null ? Constants.DEFAULT_NUM_MESSAGES
                : Integer.parseInt(cmd.getOptionValue("num_messages"));

        ArrayList<String> messages = new ArrayList<String>();
        for (int j = 0; j < num_messages; ++j) {
            messages.add(String.format("messageId = %d, timeStamp = %.3f", j, System.currentTimeMillis() / 1e+3));
        }
        writer.run(routingKey, messages);
    }

    private static Options getOptions() {
        final Options options = new Options();
        options.addOption("s", "scope", true, "The scope name of the stream to read from.");
        options.addOption("n", "name", true, "The name of the stream to read from.");
        options.addOption("u", "uri", true, "The URI to the controller in the form tcp://host:port");
        options.addOption("r", "routingKey", true, "The routing key of the message to write.");
        options.addOption("N", "num_messages", true, "Total number of messages to write.");
        return options;
    }
}
