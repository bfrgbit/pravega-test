package com.vmware.iotc.pravega_test;

import java.net.*;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.*;
import io.pravega.client.*;
import io.pravega.client.admin.*;
import io.pravega.client.stream.*;
import io.pravega.client.stream.impl.JavaSerializer;

public class SimpleWriter {
    protected final URI controllerURI;
    protected final String scope;
    protected final String streamName;

    public SimpleWriter(String scope, String streamName, URI controllerURI) {
        this.controllerURI = controllerURI;
        this.scope = scope;
        this.streamName = streamName;
    }

    public void run(String routingKey, List<String> messages) {
        StreamManager streamManager = StreamManager.create(controllerURI);

        /* final boolean scopeCreation = */ streamManager.createScope(scope);
        StreamConfiguration streamConfig = StreamConfiguration.builder().scalingPolicy(ScalingPolicy.fixed(1)).build();

        /* final boolean streamCreation = */
        streamManager.createStream(scope, streamName, streamConfig);

        try (ClientFactory clientFactory = ClientFactory.withScope(scope, controllerURI);
                EventStreamWriter<String> writer = clientFactory.createEventWriter(streamName,
                        new JavaSerializer<String>(), EventWriterConfig.builder().build())) {

            for (String message : messages) {
                System.out.format("Writing message: '%s' with routing-key: '%s' to stream '%s / %s'%n", message,
                        routingKey, scope, streamName);

                /* final CompletableFuture<Void> writeFuture = */
                writer.writeEvent(routingKey, message);
            }
        }
    }

    public void run(String routingKey, String message) {
        ArrayList<String> messages = new ArrayList<String>();
        messages.add(message);
        run(routingKey, messages);
    }

    public static void main(String[] args) {
        Options options = getOptions();
        CommandLine cmd = null;
        try {
            cmd = parseCommandLineArgs(options, args);
        } catch (ParseException e) {
            System.out.format("%s.%n", e.getMessage());
            final HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("HelloWorldWriter", options);
            System.exit(1);
        }

        final String scope = cmd.getOptionValue("scope") == null ? Constants.DEFAULT_SCOPE
                : cmd.getOptionValue("scope");
        final String streamName = cmd.getOptionValue("name") == null ? Constants.DEFAULT_STREAM_NAME
                : cmd.getOptionValue("name");
        final String uriString = cmd.getOptionValue("uri") == null ? Constants.DEFAULT_CONTROLLER_URI
                : cmd.getOptionValue("uri");
        final URI controllerURI = URI.create(uriString);

        SimpleWriter writer = new SimpleWriter(scope, streamName, controllerURI);

        final String routingKey = cmd.getOptionValue("routingKey") == null ? Constants.DEFAULT_ROUTING_KEY
                : cmd.getOptionValue("routingKey");
        final String message = cmd.getOptionValue("message") == null ? Constants.DEFAULT_MESSAGE
                : cmd.getOptionValue("message");
        writer.run(routingKey, message);
    }

    private static Options getOptions() {
        final Options options = new Options();
        options.addOption("s", "scope", true, "The scope name of the stream to read from.");
        options.addOption("n", "name", true, "The name of the stream to read from.");
        options.addOption("u", "uri", true, "The URI to the controller in the form tcp://host:port");
        options.addOption("r", "routingKey", true, "The routing key of the message to write.");
        options.addOption("m", "message", true, "The message to write.");
        return options;
    }

    protected static CommandLine parseCommandLineArgs(Options options, String[] args) throws ParseException {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);
        return cmd;
    }
}
